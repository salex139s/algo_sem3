#include <iostream>
#include <string>
#include <vector>

namespace Utils {
template <typename T>
void PrintVector(const T& elem) {
  std::cout << elem;
}

template <typename Internal>
void PrintVector(const std::vector<Internal>& vector) {
  if (vector.empty()) {
    return;
  }
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    std::cout << vector[index] << ' ';
  }
  std::cout << vector[index];
}

template <typename Internal>
void PrintVector(const std::vector<std::vector<Internal>>& vector) {
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    PrintVector(vector[index]);
    std::cout << '\n';
  }
  PrintVector(vector[index]);
}

template <typename Internal1, typename Internal2>
void PrintVector(const std::vector<std::pair<Internal1, Internal2>>& vector) {
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    PrintVector(vector[index].first);
    std::cout << ':';
    PrintVector(vector[index].second);
    std::cout << ' ';
  }
  PrintVector(vector[index].first);
  std::cout << ':';
  PrintVector(vector[index].second);
}

template <typename Internal>
void PrintlnVector(const std::vector<Internal>& vector) {
  PrintVector(vector);
  std::cout << "\n\n";
}

template <typename Integer = int>
auto StringToVector(const std::string& str, char start = char(0)) {
  std::vector<Integer> arr(str.size());
  for (int i = 0; i < static_cast<int>(str.size()); ++i) {
    arr[i] = str[i] - start;
  }
  return arr;
}

template <typename Integer>
auto VectorToString(const std::vector<Integer>& arr, char start = char(0)) {
  std::string str;
  str.resize(arr.size());
  for (size_t i = 0; i < str.size(); ++i) {
    str[i] = static_cast<char>(static_cast<Integer>(start) + arr[i]);
  }
  return str;
}

template <typename Func, typename Integer>
  requires requires(Func do_after, std::vector<Integer> count) {
    do_after(count);
  }
auto CountSort(const std::vector<Integer>& arr, Integer max_elem,
               Func do_after) {
  std::vector<Integer> count(max_elem, 0);
  for (auto index : arr) {
    ++count[index];
  }
  for (Integer i = 1; i < max_elem; ++i) {
    count[i] += count[i - 1];
  }
  do_after(count);
}
}  // namespace Utils

using LL = long long;

const LL kSigma = (1 << 20);

auto Init(const std::string& str) {
  LL length = static_cast<LL>(str.size());
  std::vector<LL> perm(length);
  std::vector<LL> classes(length, -1);

  Utils::CountSort(Utils::StringToVector<LL>(str), kSigma,
                   [&](std::vector<LL>& count) {
                     for (LL i = length - 1; i >= 0; --i) {
                       perm[--count[str[i]]] = i;
                     }
                   });

  for (LL i = 0; i < length; ++i) {
    if (i == 0) {
      classes[perm[0]] = i;
      continue;
    }
    classes[perm[i]] =
        (str[perm[i]] != str[perm[i - 1]] ? 1 : 0) + classes[perm[i - 1]];
  }

  return std::pair(std::move(classes), std::move(perm));
}

void Iteration(std::vector<LL>& classes, std::vector<LL>& perm, LL sub_len) {
  LL length = static_cast<LL>(perm.size());
  std::vector<LL> tmp_perm(length);
  std::vector<LL> tmp_classes(length);

  for (LL i = 0; i < length; ++i) {
    tmp_perm[i] = (length + perm[i] - sub_len) % length;
  }

  Utils::CountSort(classes, kSigma, [&](std::vector<LL>& count) {
    for (LL i = length - 1; i >= 0; --i) {
      perm[--count[classes[tmp_perm[i]]]] = tmp_perm[i];
    }
  });

  for (LL i = 0; i < length; ++i) {
    if (i == 0) {
      tmp_classes[perm[0]] = i;
      continue;
    }
    tmp_classes[perm[i]] =
        (classes[perm[i]] != classes[perm[i - 1]] ||
                 classes[(perm[i] + sub_len) % length] !=
                     classes[(perm[i - 1] + sub_len) % length]
             ? 1
             : 0) +
        tmp_classes[perm[i - 1]];
  }
  classes = std::move(tmp_classes);
}

auto BuildSuffixArray(const std::string& str) {
  auto [classes, perm] = Init(str);
  for (LL sub_len = 1; static_cast<LL>(str.size()) > sub_len; sub_len <<= 1) {
    Iteration(classes, perm, sub_len);
  }
  return perm;
}

void Calc(const std::string& str) {
  auto arr = BuildSuffixArray(str);
  for (auto item : arr) {
    if (item != 0) {
      std::cout << str[item - 1];
    }
  }
}

int main() {
  std::string str;
  std::cin >> str;
  Calc(str);
  return 0;
}
