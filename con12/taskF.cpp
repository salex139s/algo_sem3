#include <algorithm>
#include <bitset>
#include <cmath>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

const int kMod = 7340033;
const int kRoot = 5;
const int kInverseRoot = 4404020;
const int kPowered = 1 << 20;
const int kBarrier = 7000000;

// num <= 3 037 000 499
template <typename Integer1, typename Integer2, typename Integer3>
auto BinPow(Integer1 num, Integer2 pow, Integer3 mod) {
  long long long_num(num);
  long long long_mod(mod);
  long_num = (long_num + long_mod) % long_mod;

  if (long_mod < 2) {
    throw std::logic_error("bad mod");
  }

  if (pow < 0) {
    throw std::logic_error("bad pow");
  }

  if (long_num == 0 || long_num == 1) {
    return long_num;
  }

  long long res = 1;
  for (; pow > 0; pow >>= 1) {
    if ((pow & 1) != 0) {
      res = (res * long_num) % long_mod;
    }
    long_num = (long_num * long_num) % long_mod;
  }

  return res;
}

// num <= 3 037 000 499
template <typename Integer1, typename Integer2>
auto Inverse(Integer1 num, Integer2 mod) {
  return BinPow(num, mod - 2, mod);
}

auto Reverse(std::vector<int>& poly) {
  for (size_t i = 1, j = 0; i < poly.size(); ++i) {
    size_t bit = poly.size() >> 1;
    for (; j >= bit; bit >>= 1) {
      j -= bit;
    }
    j += bit;
    if (i < j) {
      std::swap(poly[i], poly[j]);
    }
  }
}

auto GetPowered(size_t size, bool invert) {
  long long powered = invert ? kInverseRoot : kRoot;
  for (size_t i = size; i < kPowered; i <<= 1) {
    powered = powered * powered % kMod;
  }
  return powered;
}

void DFT(std::vector<int>& poly, bool invert) {
  Reverse(poly);

  for (size_t size = 2; size <= poly.size(); size <<= 1) {
    auto powered = GetPowered(size, invert);

    for (size_t i = 0; i < poly.size(); i += size) {
      long long root = 1;
      for (size_t j = 0; j < size >> 1; ++j) {
        int tmp1 = poly[i + j];
        int tmp2 =
            static_cast<int>(1LL * poly[i + j + (size >> 1)] * root % kMod);
        poly[i + j] = tmp1 + tmp2 % kMod;
        poly[i + j + (size >> 1)] = (tmp1 - tmp2 + kMod) % kMod;
        root = root * powered % kMod;
      }
    }
  }
  if (!invert) {
    return;
  }
  int inversed = static_cast<int>(Inverse(poly.size(), kMod));
  for (int& elem : poly) {
    elem = static_cast<int>(1LL * elem * inversed % kMod);
  }
}

auto Input() {
  int len;
  std::cin >> len;
  ++len;
  std::vector<int> poly(len);
  for (int i = 0; i < len; ++i) {
    std::cin >> poly[i];
  }
  return poly;
}

auto Multiply(const std::vector<int>& poly1, const std::vector<int>& poly2) {
  std::vector<int> poly(poly1.size());
  for (size_t i = 0; i < poly1.size(); ++i) {
    poly[i] = static_cast<int>(1LL * poly1[i] * poly2[i] % kMod);
  }
  return poly;
}

auto Resize(std::vector<int>& poly1, std::vector<int>& poly2) {
  size_t size = 1;
  while (size <= poly1.size() + poly2.size()) {
    size <<= 1;
  }

  poly1.resize(size, 0);
  poly2.resize(size, 0);
}

int main() {
  auto poly1 = Input();
  auto poly2 = Input();

  auto size = poly1.size() + poly2.size() - 1;
  Resize(poly1, poly2);

  DFT(poly1, false);
  DFT(poly2, false);

  auto res = Multiply(poly1, poly2);

  DFT(res, true);

  std::cout << size - 1 << ' ';

  for (size_t i = 0; i < size; ++i) {
    std::cout << (res[i] > kBarrier ? res[i] - kMod : res[i]) << ' ';
  }

  return 0;
}
