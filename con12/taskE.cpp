#include <algorithm>
#include <bitset>
#include <cmath>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

const long long kMax = 1e9 + 7;

namespace Utils {
template <typename T>
void PrintVector(const T& elem) {
  std::cout << elem;
}

template <typename Internal>
void PrintVector(const std::vector<Internal>& vector) {
  if (vector.empty()) {
    return;
  }
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    std::cout << vector[index] << ' ';
  }
  std::cout << vector[index];
}

template <typename Internal>
void PrintVector(const std::vector<std::vector<Internal>>& vector) {
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    PrintVector(vector[index]);
    std::cout << '\n';
  }
  PrintVector(vector[index]);
}

template <typename Internal1, typename Internal2>
void PrintVector(const std::vector<std::pair<Internal1, Internal2>>& vector) {
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    PrintVector(vector[index].first);
    std::cout << ':';
    PrintVector(vector[index].second);
    std::cout << ' ';
  }
  PrintVector(vector[index].first);
  std::cout << ':';
  PrintVector(vector[index].second);
}

template <typename Internal>
void PrintlnVector(const std::vector<Internal>& vector) {
  PrintVector(vector);
  std::cout << "\n\n";
}

template <typename Integer1, typename Integer2>
auto Subtract(std::vector<Integer1>& reduced,
              const std::vector<Integer2>& subtracted) {
  for (size_t i = 0; i < reduced.size(); ++i) {
    reduced[i] -= static_cast<Integer1>(subtracted[i]);
  }
  return reduced;
}

template <typename Integer1, typename Integer2>
auto Multiply(std::vector<Integer1>& mult1,
              const std::vector<Integer2>& mult2) {
  for (size_t i = 0; i < mult1.size(); ++i) {
    mult1[i] *= static_cast<Integer1>(mult2[i]);
  }
  return mult1;
}

template <typename Integer1, typename Integer2, typename Integer3 = long long>
long long BinPow(Integer1 base, Integer2 pow, Integer3 mod = kMax) {
  long long result = 1;
  base %= mod;
  while (pow > 0) {
    if ((pow & 1) != 0) {
      result = (result * base) % mod;
    }
    base = static_cast<Integer1>((1LL * base * base) % mod);
    pow >>= 1;
  }
  return result;
}
}  // namespace Utils

using LL = long long;

auto GetDivided(LL mod) {
  LL ss;
  LL copy;
  for (ss = 0, copy = mod - 1; copy % 2 == 0; copy >>= 1) {
    ss++;
  }
  return std::pair(copy, ss);
}

void Calc(LL num, LL mod) {
  auto [mm, ss] = GetDivided(mod);

  LL powered1 = Utils::BinPow(num, mm, mod);
  LL powered2 = Utils::BinPow(num, (mm + 1) / 2, mod);
  for (; powered1 % mod != 1; powered1 %= mod, powered2 %= mod) {
    LL power = 0;
    for (LL i = 1; Utils::BinPow(powered1, i, mod) != 1; i *= 2) {
      ++power;
    }
    LL rand_num;
    for (LL i = 0; i != -1;) {
      rand_num = (random() + 1) % mod;
      i = Utils::BinPow(rand_num,
                        mm * Utils::BinPow(2LL, std::max((ss - 1), 0LL)), mod);
      i = i == mod - 1 ? -1 : i;
    }
    LL tmp1 = Utils::BinPow(rand_num, mm, mod);
    powered1 *=
        Utils::BinPow(tmp1, Utils::BinPow(2LL, std::max(ss - power, 0LL)), mod);
    powered2 *= Utils::BinPow(
        tmp1, Utils::BinPow(2LL, std::max(ss - power - 1, 0LL)), mod);
  }
  std::cout << powered2 % mod << "\n";
}

int main() {
  int count;
  std::cin >> count;
  for (int i = 0, num, mod; i < count; ++i) {
    std::cin >> num >> mod;
    if (mod == 2 || num == 0 || num == 1) {
      std::cout << num << "\n";
      continue;
    }
    if (Utils::BinPow(num, (mod - 1) / 2, mod) % mod != 1) {
      std::cout << "IMPOSSIBLE\n";
      continue;
    }
    if (mod % 4 == 3) {
      std::cout << Utils::BinPow(num, (mod + 1) / 4, mod) << "\n";
      continue;
    }
    Calc(num, mod);
  }
  return 0;
}
