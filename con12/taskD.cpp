#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <map>
#include <numeric>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

namespace Utils {
template <typename T>
void PrintVector(const T& elem) {
  std::cout << elem;
}

template <typename Internal>
void PrintVector(const std::vector<Internal>& vector) {
  if (vector.empty()) {
    return;
  }
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    std::cout << vector[index] << ' ';
  }
  std::cout << vector[index];
}

template <typename Internal>
void PrintVector(const std::vector<std::vector<Internal>>& vector) {
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    PrintVector(vector[index]);
    std::cout << '\n';
  }
  PrintVector(vector[index]);
}

template <typename Internal1, typename Internal2>
void PrintVector(const std::vector<std::pair<Internal1, Internal2>>& vector) {
  size_t index = 0;
  for (; index < vector.size() - 1; ++index) {
    PrintVector(vector[index].first);
    std::cout << ':';
    PrintVector(vector[index].second);
    std::cout << ' ';
  }
  PrintVector(vector[index].first);
  std::cout << ':';
  PrintVector(vector[index].second);
}

template <typename Internal>
void PrintlnVector(const std::vector<Internal>& vector) {
  PrintVector(vector);
  std::cout << "\n\n";
}

template <typename Integer1, typename Integer2>
auto Subtract(std::vector<Integer1>& reduced,
              const std::vector<Integer2>& subtracted) {
  for (size_t i = 0; i < reduced.size(); ++i) {
    reduced[i] -= static_cast<Integer1>(subtracted[i]);
  }
  return reduced;
}

template <typename Integer1, typename Integer2>
auto Multiply(std::vector<Integer1>& mult1,
              const std::vector<Integer2>& mult2) {
  for (size_t i = 0; i < mult1.size(); ++i) {
    mult1[i] *= static_cast<Integer1>(mult2[i]);
  }
  return mult1;
}

template <typename Integer1, typename Integer2, typename Integer3>
long long BinPow(Integer1 base, Integer2 pow, Integer3 mod) {
  long long result = 1;
  base %= mod;
  while (pow > 0) {
    if ((pow & 1) != 0) {
      result = (result * base) % mod;
    }
    base = static_cast<Integer1>((1LL * base * base) % mod);
    pow >>= 1;
  }
  return result;
}
}  // namespace Utils

struct IntM {
  int val;
  int mod;

  IntM(int val, int mod) : val(val % mod), mod(mod) {}
  IntM(long long val, int mod) : val(static_cast<int>(val % mod)), mod(mod) {}

  IntM operator*(IntM num) const {
    if (mod != num.mod) {
      throw std::logic_error("diff mods");
    }
    return {1LL * val * num.val, mod};
  }

  IntM operator+(IntM num) const {
    if (mod != num.mod) {
      throw std::logic_error("diff mods");
    }
    return {0LL + val + num.val, mod};
  }

  IntM operator-(IntM num) const {
    if (mod != num.mod) {
      throw std::logic_error("diff mods");
    }
    return {0LL + mod + val - num.val, mod};
  }

  IntM operator/(IntM num) const {
    if (mod != num.mod) {
      throw std::logic_error("diff mods");
    }
    return {1LL * Utils::BinPow(num.val, mod - 2, 1LL * mod) * val, mod};
  }

  auto operator<=>(IntM num) const {
    if (mod != num.mod) {
      throw std::logic_error("diff mods");
    }
    return val <=> num.val;
  }

  IntM operator^(IntM num) const {
    if (mod != num.mod) {
      throw std::logic_error("diff mods");
    }
    return {Utils::BinPow(val, num.val, 1LL * mod), mod};
  }

  IntM operator*(int num) const { return {1LL * val * num, mod}; }

  IntM operator+(int num) const { return {0LL + val + num, mod}; }

  IntM operator-(int num) const { return {0LL + mod + val - num, mod}; }

  IntM operator/(int num) const {
    return {1LL * Utils::BinPow(num, mod - 2, 1LL * mod) * val, mod};
  }

  auto operator<=>(int num) const { return val <=> num; }

  IntM operator^(int num) const {
    return {Utils::BinPow(val, num, 1LL * mod), mod};
  }

  operator int() const { return val; }
};

int Log(IntM base, IntM val) {
  int count = static_cast<int>(ceil(sqrt(val.mod)));
  std::unordered_map<int, int> map;
  IntM current(1, val.mod);
  map.reserve(count + 1);
  for (int j = 0; j < count; ++j) {
    map[current] = j;
    current = current * base;
  }
  base = base ^ (val.mod - count - 1);
  for (int i = 0; i < count; ++i, val = val * base) {
    if (map.find(val) != map.end()) {
      return map[val] + i * count;
    }
  }
  return 0;
}

int main() {
  int val;
  int base;
  int mod;
  while (std::cin >> mod >> base >> val) {
    if (val == 1) {
      std::cout << "0\n";
      continue;
    }
    int res = Log({base, mod}, {val, mod});
    std::cout << (res != 0 ? std::to_string(res) : "no solution") << '\n';
  }
  return 0;
}
