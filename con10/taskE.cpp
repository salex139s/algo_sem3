#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

template <char begin, char end>
class Trie {
private:
  struct Node {
    template <typename Key, typename Value>
    struct MyMap : public std::unordered_map<Key, Value> {
      auto Exist(Key key) {
        return std::unordered_map<Key, Value>::find(key) !=
               std::unordered_map<Key, Value>::end();
      }
    };

    MyMap<int, int> children;
    int depth;
    int sub_count;
    Node() : depth(0), sub_count(0) {}
  };

  std::vector<Node> nodes;

public:
  Trie() : nodes(1) {}

  void Add(const std::string& str) {
    int vert = 0;
    for (int i = 0; i < static_cast<int>(str.size());) {
      int index = (str[i] - begin) * (end - begin + 1) +
                  str[str.size() - i - 1] - begin;
      if (!nodes[vert].children.Exist(index)) {
        nodes[vert].children[index] = static_cast<int>(nodes.size());
        nodes.emplace_back();
      }
      ++nodes[vert].sub_count;
      vert = nodes[vert].children[index];
      nodes[vert].depth = ++i;
    }
    ++nodes[vert].sub_count;
  }

  auto CalcGroups(int min_size, int size) {
    std::vector<int> res(size, 0);
    for (auto& node : nodes) {
      if (node.sub_count >= min_size) {
        ++res[node.depth];
      }
    }
    return res;
  }
};

const char kSymbBegin = '0';
const char kSymbEnd = '9';

int main() {
  int count;
  int min_size;
  std::cin >> count >> min_size;

  Trie<kSymbBegin, kSymbEnd> trie;
  for (int i = 0; i < count; ++i) {
    std::string tmp;
    std::cin >> tmp;
    trie.Add(tmp);
  }

  int query_count;
  std::cin >> query_count;

  std::vector<int> depths(query_count);
  for (int i = 0; i < query_count; ++i) {
    std::cin >> depths[i];
  }

  auto groups = trie.CalcGroups(
      min_size, *std::max_element(depths.begin(), depths.end()) + 1);
  for (int depth : depths) {
    std::cout << groups[depth] << "\n";
  }
  return 0;
}
