#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

const int kAlphabetPower = 26;

class Trie {
private:
  struct Node {
    Node()
        : symbol('a'),
          parent(this),
          go(kAlphabetPower),
          suffix(nullptr),
          suffix2(nullptr) {}

    Node(Node* parent, char vert)
        : symbol(vert),
          parent(parent),
          go(kAlphabetPower),
          suffix(nullptr),
          suffix2(nullptr) {}

    std::string pattern;
    char symbol;
    Node* parent;
    std::unordered_map<int, Node*> to;
    std::vector<Node*> go;
    Node* suffix;
    Node* suffix2;
  };

  Node* root;
  size_t size;

  void RecursiveDelete(Node* vert) {
    for (auto node : vert->to) {
      if (node.second != nullptr) {
        RecursiveDelete(node.second);
      }
    }
    delete vert;
  }

  void CalcGo(Node* vert, char symbol) {
    if (vert->to[symbol - 'a'] != nullptr) {
      vert->go[symbol - 'a'] = vert->to[symbol - 'a'];
      return;
    }

    if (vert->suffix == nullptr) {
      CalcSuffix(vert);
    }
    if (vert == root) {
      vert->go[symbol - 'a'] = vert;
      return;
    }

    if (vert->suffix->go[symbol - 'a'] == nullptr) {
      CalcGo(vert->suffix, symbol);
    }
    vert->go[symbol - 'a'] = vert->suffix->go[symbol - 'a'];
  }

  void CalcSuffix(Node* vert) {
    if (vert->parent == root) {
      vert->suffix = vert->parent;
      return;
    }
    if (vert->parent->suffix == nullptr) {
      CalcSuffix(vert->parent);
      return;
    }
    if (vert->parent->suffix->go[vert->symbol - 'a'] == nullptr) {
      CalcGo(vert->parent->suffix, vert->symbol);
    }
    vert->suffix = vert->parent->suffix->go[vert->symbol - 'a'];
  }

  void CalcSuffix2(Node* node) {
    if (node->suffix == nullptr) {
      CalcSuffix(node);
    }
    auto* suffix = node->suffix;
    if (suffix == root || !suffix->pattern.empty()) {
      node->suffix2 = suffix;
      return;
    }

    if (suffix->suffix2 == nullptr) {
      CalcSuffix2(suffix);
    }
    node->suffix2 = suffix->suffix2;
  }

public:
  Trie() : root(new Node()), size(0) {}
  ~Trie() { RecursiveDelete(root); }

  auto FindString(const std::string& str) {
    std::unordered_map<std::string, std::vector<size_t>> res;
    auto* vert = root;
    for (size_t i = 0; i < str.size(); ++i) {
      if (vert->go[str[i] - 'a'] == nullptr) {
        CalcGo(vert, str[i]);
      }
      vert = vert->go[str[i] - 'a'];
      for (auto* copy = vert; copy != root; copy = copy->suffix2) {
        if (!copy->pattern.empty()) {
          res[copy->pattern].push_back(i + 1 - copy->pattern.length());
        }
        if (copy->suffix2 == nullptr) {
          CalcSuffix2(copy);
        }
      }
    }
    return res;
  }

  void Add(const std::string& pattern) {
    auto* vert = root;
    for (auto symbol : pattern) {
      if (vert->to[symbol - 'a'] == nullptr) {
        vert->to[symbol - 'a'] = new Node(vert, symbol);
      }
      vert = vert->to[symbol - 'a'];
    }
    vert->pattern = pattern;
    ++size;
  }
};

int main() {
  std::string str;
  std::cin >> str;
  Trie corasick;
  int count;
  std::cin >> count;
  std::vector<std::string> patterns(count);
  for (int i = 0; i < count; ++i) {
    std::cin >> patterns[i];
    corasick.Add(patterns[i]);
  }
  auto res = corasick.FindString(str);
  for (auto& pat : patterns) {
    std::cout << res[pat].size() << ' ';
    for (auto pos : res[pat]) {
      std::cout << pos + 1 << ' ';
    }
    std::cout << '\n';
  }
  return 0;
}
